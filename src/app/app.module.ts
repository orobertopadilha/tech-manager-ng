import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './ui/login/login.component';
import { ReservationsComponent } from './ui/admin/reservations/reservations.component';
import { HomeComponent } from "./ui/home/home.component";
import { UserReservationsComponent } from './ui/user/reservations/reservations.component';
import { AppHeaderComponent } from './ui/app-header/app-header.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './service/auth.interceptor';
import { AppMaterialModule } from './app-material/app-material.module';
import { ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { NewReservationComponent } from './ui/user/new-reservation/new-reservation.component';
import { ListReservationsComponent } from './ui/common/list-reservations/list-reservations.component';

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        HomeComponent,
        UserReservationsComponent,
        ReservationsComponent,
        AppHeaderComponent,
        NewReservationComponent,
        ListReservationsComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        AppMaterialModule,
        ReactiveFormsModule,
        HttpClientModule
    ],
    providers: [{ provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}],
    bootstrap: [AppComponent]
})
export class AppModule { }
