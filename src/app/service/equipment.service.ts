import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Equipment } from "../model/equipment.model";
import { WS_EQUIPMENT } from "./endpoints";

@Injectable(
    { providedIn: 'root' }
)
export class EquipmentService {

    constructor(private http: HttpClient) {

    }

    findAll() : Observable<Equipment[]> {
        return this.http.get<Equipment[]>(WS_EQUIPMENT)
    }

}