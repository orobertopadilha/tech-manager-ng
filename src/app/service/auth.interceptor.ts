import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { SessionService } from "./session.service";

@Injectable(
    { providedIn: 'root' }
)
export class AuthInterceptor implements HttpInterceptor {
    
    constructor(private sessionService: SessionService) {

    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (this.sessionService.isLoggedIn) {
            let authRequest = req.clone({
                headers: req.headers.set("Authorization", "Bearer " + this.sessionService.authToken)
            })

            return next.handle(authRequest)
        }

        return next.handle(req)
    }

}