const URL_BASE = "http://localhost:8080"

export const WS_LOGIN = URL_BASE + "/login"
export const WS_EQUIPMENT = URL_BASE + "/equipment"
export const WS_EQUIPMENT_TYPE = URL_BASE + "/equipment-type"

export const WS_RESERVATION = URL_BASE + "/reservation"
export const WS_MY_RESERVATIONS = URL_BASE + "/reservation/my-reservations"