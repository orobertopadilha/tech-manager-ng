import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { AuthData, AuthResult } from "../model/login.model";
import { WS_LOGIN } from "./endpoints";

@Injectable({
    providedIn: 'root'
})
export class UserService {

    constructor(private http: HttpClient) {

    }

    login(authData: AuthData): Observable<AuthResult> {
        return this.http.post<AuthResult>(WS_LOGIN, authData)
    }
}