import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { catchError, map } from "rxjs/operators";
import { RegisterReservation, Reservation } from "../model/reservation.model";
import { WS_MY_RESERVATIONS, WS_RESERVATION } from "./endpoints";

@Injectable(
    { providedIn: 'root' }
)
export class ReservationService {

    constructor(private http: HttpClient) {

    }

    findAll() : Observable<Reservation[]> {
        return this.http.get<Reservation[]>(WS_RESERVATION)
    }

    findMyReservations() : Observable<Reservation[]> {
        return this.http.get<Reservation[]>(WS_MY_RESERVATIONS)
    }

    save(reservation: RegisterReservation): Observable<any> {
        return this.http.post<RegisterReservation>(WS_RESERVATION, reservation)
    }

}