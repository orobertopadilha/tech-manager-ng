import { NgModule } from '@angular/core';
import { NavigationStart, Router, RouterModule, Routes } from '@angular/router';
import { SessionService } from './service/session.service';
import { ReservationsComponent } from './ui/admin/reservations/reservations.component';
import { LoginComponent } from './ui/login/login.component';
import { HomeComponent } from './ui/home/home.component';
import { UserReservationsComponent } from './ui/user/reservations/reservations.component';

const routes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'admin', component: HomeComponent, children: [
        { path: 'reservations', component: ReservationsComponent },
        { path: '', pathMatch: 'full', redirectTo: 'reservations' }
    ]},
    { path: 'user', component: HomeComponent, children: [
        { path: 'my-reservations', component: UserReservationsComponent },
        { path: '', pathMatch: 'full', redirectTo: 'my-reservations' }
    ]},
    { path: '', pathMatch: 'full', redirectTo: 'reservations' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { 

    constructor(private router: Router,
        private sessionService: SessionService) {
        this.setupRouterListener()
    }

    private setupRouterListener() {
        this.router.events.subscribe(event => {
            if (event instanceof NavigationStart) {
                if (!this.sessionService.isLoggedIn && event.url !== "/login") {
                    this.router.navigateByUrl('/login')
                } else if (this.sessionService.isLoggedIn) {

                    if ((!this.sessionService.isAdmin && event.url.startsWith('/admin')) ||
                        (!this.sessionService.isProfessor && event.url.startsWith('/user')) ||
                        event.url === '/login' || event.url === '/') {
                        this.goToHome()
                    }
                }
            }
        })
    }

    private goToHome() {
        if (this.sessionService.isAdmin) {
            this.router.navigateByUrl('/admin')
        }

        return this.router.navigateByUrl('/user')
    }
}
