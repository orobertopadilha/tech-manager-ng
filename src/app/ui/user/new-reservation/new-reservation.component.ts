import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Equipment } from 'src/app/model/equipment.model';
import { EquipmentService } from 'src/app/service/equipment.service';
import { ReservationService } from 'src/app/service/reservation.service';

@Component({
    selector: 'app-new-reservation',
    templateUrl: './new-reservation.component.html',
    styleUrls: ['./new-reservation.component.css']
})
export class NewReservationComponent implements OnInit {

    constructor(private dialog: MatDialogRef<NewReservationComponent>) { }

    ngOnInit() {
    }

    cancel() {
        this.dialog.close()
    }

}
