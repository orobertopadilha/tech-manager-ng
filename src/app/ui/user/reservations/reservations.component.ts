import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { NewReservationComponent } from '../new-reservation/new-reservation.component';

@Component({
  selector: 'app-user-reservations',
  templateUrl: './reservations.component.html',
  styleUrls: ['./reservations.component.css']
})
export class UserReservationsComponent implements OnInit {

    constructor(private dialog: MatDialog) { }

    ngOnInit(): void {
        
    }

    addReservation() {
        let reservationDialog = this.dialog.open(NewReservationComponent, {
            width: '300px'
        })

        reservationDialog.afterClosed().subscribe( () => {
            // atualizar a lista de reservas
        })
    }

}
