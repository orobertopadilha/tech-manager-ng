export interface Reservation {
    id: number
    user: string
    equipment: string
    date: Date
}

export interface RegisterReservation {
    equipment: number
    date: Date
}